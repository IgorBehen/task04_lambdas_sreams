package com.ihorbehen.streams_api_integers;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainStrimApiInt {

/*  Create a few methods that returns list (or array) of random
    integers. Methods should use streams API and should be
    implemented using different Streams generators.
    - Count average, min, max, sum of list values. Try to count sum using both reduce and sum Stream methods
    - Count number of values that are bigger than average
    */

    public static void main(String[] args) {
        MainStrimApiInt m = new MainStrimApiInt();

        m.streamArryAsList();
        m.streamIterated();
        m.sumByReduce();
        m.streamOfArry2();
        m.streamOfArry();
        m.summaryStream();
    }

    private void streamArryAsList() {
        Collection<Integer> collection = Arrays.asList(10, 21, 35, 42, 57, 69);
        System.out.print("All list: ");
        collection.stream().forEach(v -> System.out.print(v + " "));
        System.out.println();

        collection.stream() // average
                .mapToInt(i -> i)
                .average()
                .ifPresent(avg -> System.out.println("Average of list is -  " + avg));

        OptionalDouble averageByOptional = collection.stream() // average
                .mapToInt(i -> i)
                .average();
        System.out.println("Average by " + averageByOptional);
    }

    private void streamIterated() {
        List<Integer> streamIterated = Stream.iterate(20, n -> n + 3)
                .limit(30)
                .collect(Collectors.toList());

        streamIterated.stream() // max
                .max(Comparator.comparing(i -> i))
                .ifPresent(max -> System.out.println("Maximum of Stream Iterated List is - " + max));

        streamIterated.stream() // min
                .min(Comparator.comparing(i -> i))
                .ifPresent(min -> System.out.println("Minimum of Stream Iterated List is - " + min));

        int sum = streamIterated.stream() // sum
                .mapToInt(i -> i)
                .sum();
        System.out.println("Sum of Stream Iterated List is - " + sum);
    }

    private void sumByReduce() {
        Optional<Integer> r = Stream.of(1, 2, 3, 4, 5, 6)
                .reduce(Integer::sum);  // або .reduce((a, b) -> a + b)
        System.out.println("Sum by reduce() - " + r);
    }

    void streamOfArry2() { // reduce streamOfArray
        Stream<Integer> streamOfArray = Stream.of(33, 44, 55, 66, 77);
        Optional<Integer> intOptional = streamOfArray
                .reduce((i, j) -> (i + j));
        if (intOptional.isPresent())
            System.out.println("Sum by reduce() for streamOfArray - " + intOptional.get());
    }

    private void streamOfArry() { // All array
        Integer[] arr = new Integer[]{111, 222, 333};
        Stream<Integer> streamOfArray2 = Arrays.stream(arr);
        System.out.print("Array all numbers: ");
        streamOfArray2.forEach(p -> System.out.print(p + ", "));
        System.out.println();
    }

    private void summaryStream() {  // summary
        List<Integer> summaryInfo = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        IntSummaryStatistics stats = summaryInfo.stream()
                .mapToInt((x) -> x)
                .summaryStatistics();
        System.out.println("List " + stats);
        System.out.println();
    }
}
