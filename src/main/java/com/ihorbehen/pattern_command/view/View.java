package com.ihorbehen.pattern_command.view;

public class View {
    public void printMenu() {
        String menu = "Menu\n"
                + "1. If you want to turn right, write - 'right', and point across the space\n" +
                "number of times by number\n"
                + "2. If you want to turn left, write - 'left', and point across the space\n" +
                "number of times by number\n"
                + "3. If you want to go ahead, write - 'ahead', and point across the space\n" +
                "number of times by number\n"
                + "4. If you want to go back, write - 'back', and point across the space\n" +
                "number of times by number\n"
                + "5. If you want to quit, write - 'quit'\n";
        System.out.println(menu);
    }
}
