package com.ihorbehen.pattern_command;

import com.ihorbehen.pattern_command.view.View;
import com.ihorbehen.pattern_command.menu.Menu;

public class MainPatCom {
    public static void main(String[] args) {
        Menu m = new Menu();
        View v = new View();
        v.printMenu();
        m.menu();
    }
}
