package com.ihorbehen.pattern_command.menu;

import java.util.ArrayList;
import java.util.List;
import com.ihorbehen.pattern_command.command.Command;

import com.ihorbehen.pattern_command.command.TestDirection;

import java.util.Scanner;

public class Menu {
    private TestDirection td = new TestDirection();
    private Scanner scan = new Scanner(System.in);

    public void menu() {
        boolean isQuit = false;
        while (!isQuit) {
            String scan_str = "";
            String[] s = null;
            String splitStr1 = "";
            String splitStr2 = "";
            int num = 0;
            try {
                scan_str = scan.nextLine().toLowerCase().trim();
                s = scan_str.split(" ");
                splitStr1 = s[0];
                splitStr2 = s[1];
                num = Integer.parseInt(splitStr2);
            } catch (Exception e) {
                System.out.println("Please enter the correct values!");
            }
            if (splitStr1.equals("right")) {
                td.turnGoRight();
                System.out.print(" " + num + " times.");
            } else if (splitStr1.equals("left")) {
                td.turnGoLeft();
                System.out.print(" " + num + " times.");
            } else if (splitStr1.equals("ahead")) {
                td.turnGoAhead();
                System.out.print(" " + num + " times.");
            } else if (splitStr1.equals("back")) {
                td.turnGoBack();
                System.out.print(" " + num + " times.");
            } else if (splitStr1.equals("quit")) {
                isQuit = true;
            }
        }scan.close();
    }
    /*public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Command> availableCommands = new ArrayList<>();
        int scan1 = scan.nextInt();

        availableCommands.add(new TestDirection()::turnGoAhead);
        availableCommands.add(new TestDirection()::turnGoLeft);
        availableCommands.add(new TestDirection()::turnGoBack);
        availableCommands.add(new TestDirection()::turnGoRight);

        for (int i = 0; i < availableCommands.size(); i++) {

            if (scan1 == i) {

                System.out.println(availableCommands.get(i));
            }
        }
    }*/

}