package com.ihorbehen.pattern_command.command;

class Direction {

    Direction() {
    }

    void turnLeft() {
        System.out.print("You turned left");
    }

    String turnRight() {
        return "You turned right";
    }

    String goForward() {
        return "You go ahead";
    }

    void goBack() {
        System.out.print("You go back");
    }
}
