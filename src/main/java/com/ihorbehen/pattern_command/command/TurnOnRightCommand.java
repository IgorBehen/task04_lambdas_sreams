package com.ihorbehen.pattern_command.command;

public class TurnOnRightCommand implements Command {
    private Direction theDirection;

    TurnOnRightCommand(Direction direction) {
        this.theDirection = direction;
    }

    @Override
    public void execute() {
        theDirection.turnRight();
    }
}

