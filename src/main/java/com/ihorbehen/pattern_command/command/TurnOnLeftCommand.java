package com.ihorbehen.pattern_command.command;

public class TurnOnLeftCommand implements Command {
    private Direction theDirection;

    TurnOnLeftCommand(Direction direction) {
        this.theDirection = direction;
    }

    @Override
    public void execute() {
        theDirection.turnLeft();
    }

}
