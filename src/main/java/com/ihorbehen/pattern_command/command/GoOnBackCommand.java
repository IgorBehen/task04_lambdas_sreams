package com.ihorbehen.pattern_command.command;

public class GoOnBackCommand implements Command {
    private Direction theDirection;

    GoOnBackCommand(Direction direction) {
        this.theDirection = direction;
    }

    @Override
    public void execute() {
        theDirection.goBack();
    }
}
