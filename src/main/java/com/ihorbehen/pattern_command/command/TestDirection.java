package com.ihorbehen.pattern_command.command;

public class TestDirection {
    private Direction dir = new Direction();
    private Command veerLeft = new TurnOnLeftCommand(dir);
    private Command veerRight = new TurnOnRightCommand(dir);
    private Command veerForward = new GoOnForwardCommand(dir);
    private Command veerBack = new GoOnBackCommand(dir);
    private CommandInvoker ci = new CommandInvoker(veerLeft, veerRight, veerForward, veerBack);

    public void turnGoAhead() {
        Command turnGoAhead = () -> {
            String s = dir.goForward();
            System.out.println(s);
        };
        turnGoAhead.execute();
    }

    public void turnGoLeft() {
        ci.flipLeft();  //ci.flipRight(); //ci.flipForward();
    }

    public void turnGoBack() {
        ci.flipBack();
    }

    public void turnGoRight() {
        Command turnedRight = new Command() {
            @Override
            public void execute() {
                String s1 = dir.turnRight();
                System.out.println(s1);
            }

        };
        turnedRight.execute();
    }

//    void turnGoBack() {
//        Command turnGoBack = dir::goBack; //??? ci::flipLeft;
//        System.out.println(turnGoBack);
//    }

}

