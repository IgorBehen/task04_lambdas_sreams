package com.ihorbehen.pattern_command.command;

public interface Command {
    void execute();
}
