package com.ihorbehen.pattern_command.command;

public class GoOnForwardCommand implements Command {
    private Direction theDirection;

    GoOnForwardCommand(Direction direction) {
        this.theDirection = direction;
    }

    @Override
    public void execute() {
        theDirection.goForward();
    }
}
