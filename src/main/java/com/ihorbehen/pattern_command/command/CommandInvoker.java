package com.ihorbehen.pattern_command.command;

public class CommandInvoker {
    private Command flipLeftCommand;
    private Command flipRightCommand;
    private Command flipForwardCommand;
    private Command flipBackCommand;

    public CommandInvoker(Command flipLeftCommand, Command flipRightCommand, Command flipForwardCommand, Command flipBackCommand) {
        this.flipLeftCommand = flipLeftCommand;
        this.flipRightCommand = flipRightCommand;
        this.flipForwardCommand = flipForwardCommand;
        this.flipBackCommand = flipBackCommand;
    }

    public void flipLeft() {
        flipLeftCommand.execute();
    }

    public void flipRight() {
        flipRightCommand.execute();
    }

    public void flipForward() {
        flipForwardCommand.execute();
    }

    public void flipBack() {
        flipBackCommand.execute();
    }
}
