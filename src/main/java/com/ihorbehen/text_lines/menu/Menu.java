package com.ihorbehen.text_lines.menu;

import com.ihorbehen.text_lines.logic.TextLinesLogic;
import com.ihorbehen.text_lines.view.View;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    TextLinesLogic tll = new TextLinesLogic();
    public void menu() {
        tll.fillingList(tll.text);
        int isQuit = 1;
        while (isQuit != 0) {
            View.printMenu();
            int choice = 0;
            try {
                choice = scanner.nextInt();
            } catch (Exception e) {
              System.out.println("Please enter the correct value!");
            }
            switch (choice) {
                case 1:
                    tll.countOfAllWords(tll.text);
                    break;
                case 2:
                    tll.countOfEachWord(tll.text);
                    break;
                case 3:
                    tll.countUniqueWords(tll.text);
                    break;
                case 4:
                    tll.sortedListOfUniqueWords(tll.text);
                    break;
                case 5:
                    tll.numberOfEachCharacter(tll.text);
                    break;
                case 0:
                    isQuit = 0;
                    break;
                default:
                    System.out.println("Please enter the correct value!!!");
            }
        }
    }
}