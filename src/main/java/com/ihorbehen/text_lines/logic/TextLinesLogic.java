package com.ihorbehen.text_lines.logic;

import com.ihorbehen.text_lines.MainOfTextLines;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class TextLinesLogic {
    public List<String> text = new LinkedList<>();

    public void fillingList(List<String> text) {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        System.out.println("Please insert text and press Enter after each line: \n" + "For Quit press - 0.");
        while(true) {
            String nextLine = sc.nextLine();
            if (nextLine.equals("")) {
                continue;
            }
            if (nextLine.equals("0".toLowerCase())) {
                break;
            }
            text.add(nextLine);
            i++;
        }
    }
    public void countOfEachWord(List<String> text) {
        Map<Object, Integer> counts = text.parallelStream().
                collect(Collectors.toConcurrentMap(
                        w -> w, w -> 1, Integer::sum));
        System.out.print("Count Of Each Word:" + counts);
        System.out.println();
    }

    public void countOfAllWords(List<String> text) {
        long count = text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .count();
        System.out.println("Count of all words: " + count);
    }

    public void countUniqueWords(List<String> text) {
        long count = text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .distinct()
                .count();
        System.out.println("Count of unique words: " + count);
    }

    public void sortedListOfUniqueWords(List<String> text) {
        Map<String, Integer> frequencyMap = text.stream()
                .collect(groupingBy(Function.identity(), Collectors.summingInt(val -> 1)));
        Map<String, Integer> result =  frequencyMap .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        System.out.println("Sorted List Of Unique Words:");
        result.keySet().stream()
                .forEach(System.out::println);
    }

    public void numberOfEachCharacter(List<String> text) {
        List<String> c = text.stream()
                .flatMap(e -> Stream.of(e.split("")))
                .collect(Collectors.toList());
        Map<Object, Integer> counts = c.parallelStream().
                collect(Collectors.toConcurrentMap(
                        w -> w, w -> 1, Integer::sum));
        System.out.println("Number Of Each Character: " + counts);
    }
}
