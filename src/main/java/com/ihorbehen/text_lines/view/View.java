package com.ihorbehen.text_lines.view;

public class View {
    public static void printMenu() {
        String menu = "Menu\n"
                + "1. If you want to know count of all words, press            - 1\n"
                + "2. If you want to know count of each word, press            - 2\n"
                + "3. If you want to know count of unique words, press         - 3\n"
                + "4. If you want to see a sorted list of unique words, press  - 4 \n"
                + "5. If you want to see a number of each character, press     - 5 \n"
                + "6. If you want to quit, press                               - 0 \n";
        System.out.println(menu);
    }
}
