package com.ihorbehen.functional_interface_ints;

import com.ihorbehen.functional_interface_ints.MyInterface;

public class MyInterfaceClass implements MyInterface {


    public static void main(String[] args) {
        MyInterface addInts = (a, b, c) -> a + b + c;
        System.out.println(addInts.threeInts(1, 2, 3));

        MyInterface maxInts = (a, b, c) -> largestOfThree(a, b, c); // or  MyInterface maxInts = (a, b, c) -> MyInterfaceClass::largestOfThree;
        System.out.println(maxInts.threeInts(14, 15, 16));

        MyInterface averageInts = (a, b, c) -> (a + b + c) / 3;
        System.out.println(averageInts.threeInts(21, 22, 23));
    }

    @Override
    public int threeInts(int a, int b, int c) {
        return 0;
    }

    private static int largestOfThree(int first, int second, int third) {
        if (first > second && first > third) {
            return first;
        } else if (second > first && second > third) {
            return second;
        } else {
            return third;
        }
    }
}

